function Person(user, job, level) {
  this.user = user;
  this.job = job;
  this.level = level;
  this.health = level * 121;
  this.atk = level * 3.14;
  if(job == "Paladin"){
  	this.abilities = ["Crusader Strike", "Hammer of Wrath"];
  }
  else if (job == "Death knight"){
  	this.abilities = ["Death Grip", "Death Strike"];
  }
  else if (job == "Hunter"){
  	this.abilities = ["Kill Command", "Call Pet"];
  }
  else if (job == "Mage"){
  	this.abilities = ["Frost Nova", "Pyroblast"];
  }

}

let char1 = new Person("Pemperu", "Paladin" , 85);
let char2 = new Person("Roijan231" , "Death knight", 57);
let char3 = new Person("Pape144" , "Hunter", 87);
let char4 = new Person("MageAko" , "Mage", 47);

console.log(char1);
console.log(char2);
console.log(char3);
console.log(char4);