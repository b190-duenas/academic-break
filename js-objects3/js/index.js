let recipe = {
  title: "Mole",
  servings: 2,
  ingredients: ["cinnamon", "cumin", "cocoa"]
}

console.log(recipe.title);
console.log("Serves: " + recipe.servings);
console.log("Ingredients:");
for(let x = 0; x<recipe.ingredients.length; x++){
  console.log(recipe.ingredients[x]);
}

let book = [{
  title: "Harry Potter and the Goblet of Fire",
  author: "J.K. Rowling",
  alreadyRead: true
},
{
  title: "Angels & Demons",
  author: "Dan Brown",
  alreadyRead: true
},
{
  title: "Hunger Games",
  author: "Suzanne Collins",
  alreadyRead: false
}
]
for(let x=0; x<book.length; x++){
  let temp = book[x];
  let temp2 = temp.title + '" by ' + temp.author;
  if (temp.alreadyRead == true) {
    console.log('You already read "' + temp2);
  }
  else{
    console.log('You still need to read "' + temp2);
  }
}
