let recommendBooks = {
	book1: {
		author: "J.K. Rowling",
		title: "Harry Potter and the Goblet of Fire",
		datePublish: "July 2008",
		summary: "It follows Harry Potter, a wizard in his fourth year at Hogwarts School of Witchcraft and Wizardry, and the mystery surrounding the entry of Harry's name into the Triwizard Tournament, in which he is forced to compete."
	},

	book2: {
		author: "Dan Brown",
		title: "Angels & Demons",
		datePublish: "May 2000",
		summary: "When world-renowned Harvard symbologist Robert Langdon is summoned to a Swiss research facility to analyze a mysterious symbol — seared into the chest of a murdered physicist — he discovers evidence of the unimaginable: the resurgence of an ancient secret brotherhood known as the Illuminati… the most powerful ..."
	},
	
	book3: {
		author: "Dan Brown",
		title: "The Da Vince Code",
		datePublish: "April 2003",
		summary: "The Da Vinci Code follows symbologist Robert Langdon and cryptologist Sophie Neveu after a murder in the Louvre Museum in Paris causes them to become involved in a battle between the Priory of Sion and Opus Dei over the possibility of Jesus Christ and Mary Magdalene having had a child together."
	}
}

console.log(recommendBooks);